"""
XML to JSON Converter Application

This application provides a graphical user interface (GUI) to convert XML files to JSON format.
It allows users to select individual XML files or a folder containing XML files for conversion.
The conversion process runs in a separate thread to keep the GUI responsive.

The application uses the following libraries:
- PyQt6 for the GUI
- xmltodict for converting XML to a Python dictionary
- json for converting Python dictionaries to JSON
- lxml for parsing XML
- chardet for detecting the encoding of the XML files
- threading for running the conversion process in a separate thread
- pathlib for handling file and folder paths

The application consists of two main classes:
- XMLToJsonConverter: This class handles the conversion of XML to JSON.
- XMLConverterApp: This class handles the GUI and user interactions.

The application starts by creating an instance of the XMLConverterApp class and showing the window.
"""
import json
import sys
import threading
from pathlib import Path

import chardet
import xmltodict
from lxml import etree
from PyQt6.QtWidgets import (
    QApplication,
    QFileDialog,
    QLabel,
    QPushButton,
    QVBoxLayout,
    QWidget,
)


class XMLToJsonConverter:
    """
    A class used to convert XML files to JSON format.

    ...

    Attributes
    ----------
    input_path : Path
        a Path object representing the location of the input XML file
    output_folder : Path
        a Path object representing the location of the output folder

    Methods
    -------
    convert():
        Converts the XML file to JSON format.
    detect_encoding():
        Detects the encoding of the XML file.
    """

    def __init__(self, input_path, output_folder):
        """
        Constructs all the necessary attributes for the XMLToJsonConverter object.

        Parameters
        ----------
            input_path : str
                the location of the input XML file
            output_folder : str
                the location of the output folder
        """
        self.input_path = Path(input_path)
        self.output_folder = Path(output_folder)

    def convert(self):
        """
        Converts the XML file to JSON format.
        """
        encoding = self.detect_encoding()

        # Parse the XML file
        with self.input_path.open(encoding=encoding) as f:
            xml_data = etree.parse(f)

        # Convert the XML data to JSON
        json_data = json.dumps(
            xmltodict.parse(etree.tostring(xml_data)),
            indent=4, ensure_ascii=False)

        # Define the output JSON file path
        json_file_path = self.output_folder / f"{self.input_path.stem}.json"

        # Write the JSON data to the output file
        with json_file_path.open("w", encoding="utf-8") as f:
            f.write(json_data)

    def detect_encoding(self):
        """
        Detects the encoding of the XML file.

        Returns
        -------
        str
            the encoding of the XML file
        """
        with self.input_path.open('rb') as f:
            encoding = chardet.detect(f.read())['encoding']
        return encoding


class XMLConverterApp(QWidget):
    """
    A class used to create a GUI for the XML to JSON converter.

    ...

    Methods
    -------
    init_ui():
        Initializes the user interface.
    browse():
        Opens a file dialog for the user to select XML files to convert.
    browse_folder():
        Opens a folder dialog for the user to select a folder of XML files to convert.
    convert_files(file_paths, output_folder_path):
        Converts the selected XML files to JSON format.
    update_progress(progress):
        Updates the progress label in the user interface.
    """

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        """
        Initializes the user interface.
        """
        # Set up the label and buttons
        self.label = QLabel(self)
        self.label.setText(
            "Select an XML 1.4 file or a folder to convert to JSON")

        self.button = QPushButton('Browse File', self)
        self.button.clicked.connect(self.browse)

        self.button_folder = QPushButton('Browse Folder', self)
        self.button_folder.clicked.connect(self.browse_folder)

        # Set up the layout
        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.button)
        layout.addWidget(self.button_folder)
        self.setLayout(layout)

    def browse(self):
        """
        Opens a file dialog for the user to select XML files to convert.
        """
        # Set up the file dialog
        file_dialog = QFileDialog(self)
        file_dialog.setFileMode(QFileDialog.FileMode.ExistingFiles)
        file_dialog.setNameFilter("XML files (*.xml)")
        if file_dialog.exec():
            file_paths = file_dialog.selectedFiles()

            # Create the output folder
            output_folder_path = Path(
                file_paths[0]).parent / "JSON Conversions"
            output_folder_path.mkdir(exist_ok=True)

            # Start the conversion process in a new thread
            t = threading.Thread(target=self.convert_files,
                                 args=(file_paths, output_folder_path))
            t.start()

    def browse_folder(self):
        """
        Opens a folder dialog for the user to select a folder of XML files to convert.
        """
        if folder_dialog := QFileDialog.getExistingDirectory(
            self, "Select Folder"
        ):
            # Create the output folder
            folder_path = Path(folder_dialog)
            output_folder_path = folder_path / "JSON Conversions"
            output_folder_path.mkdir(exist_ok=True)

            # Find all XML files in the selected folder
            xml_files = list(folder_path.glob("*.xml"))

            # Start the conversion process in a new thread
            t = threading.Thread(target=self.convert_files,
                                 args=(xml_files, output_folder_path))
            t.start()

    def convert_files(self, file_paths, output_folder_path):
        """
        Converts the selected XML files to JSON format.

        Parameters
        ----------
            file_paths : list
                a list of file paths to the XML files to convert
            output_folder_path : Path
                a Path object representing the location of the output folder
        """
        num_files = len(file_paths)
        for i, file_path in enumerate(file_paths):
            # Convert each file
            converter = XMLToJsonConverter(file_path, output_folder_path)
            converter.convert()

            # Update the progress
            progress = int((i + 1) / num_files * 100)
            self.update_progress(progress)

        self.label.setText("Conversion finished.")

    def update_progress(self, progress):
        """
        Updates the progress label in the user interface.

        Parameters
        ----------
            progress : int
                the current progress percentage
        """
        self.label.setText(f"Converting files... {progress}%")


if __name__ == '__main__':
    # Create the application and show the window
    app = QApplication(sys.argv)
    app.setApplicationName("XML 1.4 to JSON")
    ex = XMLConverterApp()
    ex.show()
    sys.exit(app.exec())
