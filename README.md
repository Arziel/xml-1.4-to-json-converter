# XML 1.4 to JSON Converter

This is a simple GUI-based application that converts XML 1.4 files to JSON. It can also convert the contents of entire folders.

Building the script into a Windows executable
The recommended way to build the script into a Windows executable is by using `cx_Freeze`.

## To build the executable

Install [cx_Freeze](https://cx-freeze.readthedocs.io/en/latest/) by running the following command in the command line:

```bash
pip install cx_Freeze
```

Create a `setup.py` file in the same directory as the main script.
Copy the following code into the `setup.py` file:

```python
import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name="XML 1.4 to JSON Converter",
    options={"build_exe": build_exe_options},
    executables=[Executable("main.py", base=base)]
)
```

Make sure to replace "main.py" with the name of your main script file.
Navigate to the directory containing the `setup.py` file and the main script file in the command line.
Run the following command to build the executable:

```bash
python setup.py build
```

You can further optimize the .exe file using UPX by downloading the UPX executable from [https://upx.github.io](https://upx.github.io/ ) and placing it in the same directory as the `main.exe` file generated previously. In the command prompt, navigate to the build directory and run the following command to optimize the .exe file:

```bash
.\upx --best --lzma *.exe
```

The optimized executable can be found in the build directory.

## Requirements

Use [Poetry](https://python-poetry.org/docs/) to handle the project's requirements and virtual environment.

## Usage

To use the application, run the executable or run python main.py in the command line. Select an XML 1.4 file or folder to convert to JSON. The converted files will be saved in a folder named "JSON Conversions" in the same location as the input file or folder.

A sample `People.xml` file and its output `People.json` are provided inside the "example" folder.

## Fun facts

- For a while I had the need to create a script to convert XML files to JSON to make the data within does files easily usable anywhere, specially web apps.

- As an experiment, I made this using ChatGPT (free plan, GPT-3.5 turbo), march 14th 2023 Version.
It took a few tries and rewriting of some prompts, but the results gave me a good enough template to slightly edit to polish it into the final version.

- Today, march 30th 2023, I experimented again and asked GPT-3.5 turbo to refactor the code considering this list of good [programming principles](https://github.com/webpro/programming-principles), it definitely made the code clearer to read.

  - It tends to generate new names from time to time to already defined functions and classes. It also shuffles the imports each time.

  - I'll try GPT-4 when the service comes back online.

- GPT-4 is back online! So I asked it to refactor the code considering the mentioned principles. It's answer was noticeably more fluent and better explained. It seemed to analyse the principles and evaluate if they were applicable to the code or not. Then it outputted the final result which I tagged v1.3.

- This was a fun experiment and I will further optimize the code as new GPT versions are released to see what it can come up with.
